package com.holaexample.agendaarraylist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final ArrayList<Contacto> listContacto = new ArrayList<>();
    private EditText txtnombre;
    private EditText txttel1;
    private EditText txttel2;
    private EditText txtDomicilio;
    private EditText txtnotas;
    private CheckBox chkFavorito;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnListar;
    private Button btnSalir;
    Contacto savedContent;
    int savedIndex;
    int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnSalir = findViewById(R.id.btnSalir);
        txtnombre = findViewById(R.id.txtnombre);
        txttel1 = findViewById(R.id.txttel);
        txttel2 = findViewById(R.id.txttel2);
        txtDomicilio = findViewById(R.id.txtDomicilio);
        txtnotas = findViewById(R.id.txtnotas);
        chkFavorito = findViewById(R.id.chkFavorito);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnListar = findViewById(R.id.btnListar);

        btnGuardar.setOnClickListener(this.btnGuardarAction());
        btnListar.setOnClickListener(this.btnListarAction());
        btnLimpiar.setOnClickListener(this.btnLimpiarAction());
        btnSalir.setOnClickListener(this.btnSalirAction());


    }

    private View.OnClickListener btnSalirAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        };
    }


    private View.OnClickListener btnListarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("contactos", listContacto);
                intent.putExtras(bundle);
                startActivityForResult(intent, 0);
            }
        };
    }

    private View.OnClickListener btnLimpiarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        };
    }
    private void limpiar(){
        txtDomicilio.setText("");
        txtnombre.setText("");
        txttel1.setText("");
        txttel2.setText("");
        txtnotas.setText("");
        chkFavorito.setChecked(false);
    }

    private View.OnClickListener btnGuardarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validar())
                {
                    Contacto contacto = new Contacto();
                    contacto.setNombre(getText(txtnombre));
                    contacto.setDireccion(getText(txtDomicilio));
                    contacto.setNotas(getText(txtnotas));
                    contacto.setTelefono1(getText(txttel1));
                    contacto.setTelefono2(getText(txttel2));
                    contacto.setFavorito(chkFavorito.isChecked());
                    if(savedContent != null)
                    {
                        listContacto.set(savedIndex, contacto);

                        savedContent = null;

                    }
                    else
                    {
                        listContacto.add(index, contacto);
                        index++;
                    }
                    limpiar();

                    Toast.makeText(getApplicationContext(), R.string.mensaje,
                            Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(getApplicationContext(), R.string.msgerror,
                            Toast.LENGTH_SHORT).show();
            }
        };
    }

    private boolean validar(){
        if(validarCampo(txtDomicilio) || validarCampo(txtnombre) ||
                validarCampo(txtnotas) || validarCampo(txttel1) || validarCampo(txttel2))
            return false;
        else
            return true;
    }


    private boolean validarCampo(EditText txt)
    {
        return txt.getText().toString().matches("");
    }

    private String getText(EditText txt)
    {
        return txt.getText().toString();
    }

    @Override
    protected void onActivityResult(int res, int result, Intent intent){
        if(intent != null)
        {
            Bundle bundle = intent.getExtras();
            savedContent = (Contacto) bundle.getSerializable("contacto");
            savedIndex = bundle.getInt("index");
            txtnombre.setText(savedContent.getNombre());
            txtnotas.setText(savedContent.getNotas());
            txttel1.setText(savedContent.getTelefono1());
            txttel2.setText(savedContent.getTelefono2());
            txtDomicilio.setText(savedContent.getDireccion());
            chkFavorito.setChecked(savedContent.isFavorito());
        }
    }
}



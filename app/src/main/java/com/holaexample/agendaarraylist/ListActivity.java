package com.holaexample.agendaarraylist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    private TableLayout tbLista;
    private ArrayList<Contacto> contactos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        tbLista = findViewById(R.id.tbList);
        Bundle bundle = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundle.getSerializable("contactos");
        cargarContactos();
    }

    private void cargarContactos()
    {
        for(int x = 0; x < contactos.size(); x++)
        {
            final int position = x;
            final Contacto contacto = contactos.get(x);
            TableRow row = new TableRow(this);
            TextView lbNombre = new TextView(this);
            lbNombre.setText(contacto.getNombre());
            lbNombre.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            //lbNombre.setTextColor((contacto.isFavorito()) ? Color.BLUE: Color.BLACK);
            row.addView(lbNombre);
            Button btnVer = new Button(this);
            btnVer.setText(R.string.accver);
            btnVer.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
           // btnVer.setTextColor(Color.BLACK);

            btnVer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto", (Contacto) v.getTag(R.string.contacto_g));
                    bundle.putInt("index", Integer.parseInt(v.getTag(R.string.index).toString()));
                    i.putExtras(bundle);
                    setResult(RESULT_OK, i);
                    finish();
                }
            });

            btnVer.setTag(R.string.contacto_g, contacto);
            btnVer.setTag(R.string.index, x);
            row.addView(btnVer);
            tbLista.addView(row);
        }
    }
}
